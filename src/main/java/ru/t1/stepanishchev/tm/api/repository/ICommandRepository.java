package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();
}
